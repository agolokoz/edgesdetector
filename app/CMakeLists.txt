cmake_minimum_required(VERSION 3.4.1)
project(native-lib)

set(PROJECT_DIR ${CMAKE_CURRENT_LIST_DIR})


# OpenCV
add_library(opencv_core STATIC IMPORTED)
set_target_properties(opencv_core PROPERTIES IMPORTED_LOCATION ${PROJECT_DIR}/src/main/jniLibs/${ANDROID_ABI}/libopencv_core.a)

add_library(opencv_imgproc STATIC IMPORTED)
set_target_properties(opencv_imgproc PROPERTIES IMPORTED_LOCATION ${PROJECT_DIR}/src/main/jniLibs/${ANDROID_ABI}/libopencv_imgproc.a)


# 3rd Party for OpenCV
add_library(tbb STATIC IMPORTED)
set_target_properties(tbb PROPERTIES IMPORTED_LOCATION ${PROJECT_DIR}/src/main/jniLibs/${ANDROID_ABI}/libtbb.a)


# Source
include_directories(${PROJECT_DIR}/src/main/cpp/include)
include_directories(${PROJECT_DIR}/src/main/cpp/utils)

set(SOURCES
        ${PROJECT_DIR}/src/main/cpp/native-lib.cpp
        ${PROJECT_DIR}/src/main/cpp/utils/FrameController.cpp
        ${PROJECT_DIR}/src/main/cpp/utils/GeometryUtils.cpp)

add_library(native-lib SHARED ${SOURCES})

target_link_libraries(native-lib
                        log
                        z
                        opencv_imgproc
                        opencv_core
                        tbb)
