#include <jni.h>
#include <FrameController.hpp>
#include <opencv/cv.hpp>
#include <GeometryUtils.hpp>

FrameController *frameController = nullptr;

extern "C"
JNIEXPORT void JNICALL
Java_com_glacoste_edgesdetector_MainActivity_cameraInit(
        JNIEnv *env, jclass type, jint width, jint height, jint displayOrientation)
{
    if (frameController != nullptr) {
        delete frameController;
    }
    frameController = new FrameController(displayOrientation);
}

extern "C"
JNIEXPORT jintArray JNICALL
Java_com_glacoste_edgesdetector_MainActivity_findEdges(
        JNIEnv *env, jclass type, jbyteArray byteArray_, jint width, jint height)
{
    jintArray result = env->NewIntArray(0);
    if (frameController == nullptr) {
        return result;
    }

    jbyte *byteArray = env->GetByteArrayElements(byteArray_, 0);
    std::vector<std::vector<cv::Point>> contours = frameController->getFrameContours(reinterpret_cast<char *>(byteArray), width, height);
    env->ReleaseByteArrayElements(byteArray_, byteArray, 0);

    std::vector<int> shapes;
    std::vector<cv::Point> approx;
    for (size_t i = 0; i < contours.size(); ++i) {
        cv::Mat contoursMat(contours[i]);
        cv::approxPolyDP(contoursMat, approx, cv::arcLength(contoursMat, true)*0.02, true);

        if (std::fabs(cv::contourArea(contoursMat)) < 200
                 || !cv::isContourConvex(approx)
                 || GeometryUtils::isSegmentsIntersect(contours[i])) {
            continue;
        }

        shapes.push_back(static_cast<int>(approx.size()));
        for (size_t j = 0; j < approx.size(); ++j) {
            shapes.push_back(approx[j].x);
            shapes.push_back(approx[j].y);
        }
    }

    jsize arraySize = static_cast<jsize>(shapes.size());
    result = env->NewIntArray(arraySize);
    env->SetIntArrayRegion(result, 0, arraySize, shapes.data());
    return result;
}

