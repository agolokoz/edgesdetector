#include "GeometryUtils.hpp"

bool GeometryUtils::isSegmentsIntersect(const std::vector<cv::Point> &segments) {
    for (size_t i = 1; i < segments.size()-2; ++i) {
        for (size_t j = i; j < segments.size()-1; ++j) {
            if (isSegmentsIntersect(segments[i], segments[i-1], segments[j+1], segments[j])) {
                return true;
            }
        }
        if (isSegmentsIntersect(segments[i], segments[i-1], segments[segments.size()-1], segments[0])) {
            return true;
        }
    }
    return false;
}

bool GeometryUtils::isSegmentsIntersect(const cv::Point &point1,
                                        const cv::Point &point2,
                                        const cv::Point &point3,
                                        const cv::Point &point4) {
    int A1 = subtractRedundant(point1.y - point2.y);
    int A2 = subtractRedundant(point3.y - point4.y);
    int B1 = subtractRedundant(point2.x - point1.x);
    int B2 = subtractRedundant(point4.x - point3.x);
    int C1 = subtractRedundant(-A1*point1.x - B1*point1.y);
    int C2 = subtractRedundant(-A2*point3.x - B2*point3.y);

    int det = det2x2(A1, B1, A2, B2);
    if (det != 0) {
        double x = -det2x2(C1, B1, C2, B2) * 1. / det;
        double y = -det2x2(A1, C1, A2, C2) * 1. / det;
        return isBetween(point1.x, point2.x, x)
               && isBetween(point1.y, point2.y, y)
               && isBetween(point3.x, point4.x, x)
               && isBetween(point3.y, point4.y, y);
    }

    return det2x2(A1, C1, A2, C2) == 0
           && det2x2(B1, C1, B2, C2) == 0
           && isIntersect(point1.x, point2.x, point3.x, point4.x)
           && isIntersect(point1.y, point2.y, point3.y, point4.y);

}

int GeometryUtils::det2x2(int a, int b, int c, int d) {
    return a*d - b*c;
}

int GeometryUtils::subtractRedundant(int A) {
    return (A > 0) ? A-2 : A+2;
}

bool GeometryUtils::isBetween(int a, int b, double c) {
    return std::min(a, b) <= c + EPS && c <= std::max(a, b) + EPS;
}

bool GeometryUtils::isIntersect(int a, int b, int c, int d) {
    if (a > d) std::swap(a, b);
    if (c > d) std::swap(c, d);
    return std::max(a, c) <= std::min(b, d);
}











