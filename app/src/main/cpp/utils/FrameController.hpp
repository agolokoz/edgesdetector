#ifndef __FrameController_hpp__
#define __FrameController_hpp__

#include <opencv2/core/mat.hpp>
#include <opencv2/imgcodecs.hpp>

class FrameController {
public:
    FrameController(int displayOrientation);
    ~FrameController();

    std::vector<std::vector<cv::Point>> getFrameContours(char *data, int width, int height);

private:
    void clear();

    const cv::Size* _kSize          = nullptr;
    cv::Mat*        _currentFrame   = nullptr;

    int _displayOrientation = 0;
};

#endif // __FrameController_hpp__