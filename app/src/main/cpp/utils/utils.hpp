#ifndef __Utils_hpp__
#define __Utils_hpp__

#include <android/log.h>
#include <jni.h>

#define  LOG_TAG    "EdgesDetector"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)

unsigned char* unsignedCharArrayFromJByteArray(JNIEnv *env, jbyteArray array, int &length) {
    length = env->GetArrayLength(array);
    unsigned char *result = new unsigned char [length];
    env->GetByteArrayRegion(array, 0, length, reinterpret_cast<jbyte*>(result));
    return result;
}

#endif // __Utils_hpp__