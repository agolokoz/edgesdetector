#ifndef __GeometryUtils_hpp__
#define __GeometryUtils_hpp__

#include <opencv2/core/types.hpp>

class GeometryUtils {
public:
    static bool isSegmentsIntersect(const std::vector<cv::Point> &segments);

private:
    static bool isSegmentsIntersect(const cv::Point& point1,
                                    const cv::Point& point2,
                                    const cv::Point& point3,
                                    const cv::Point& point4);
    static inline int det2x2(int a, int b, int c, int d);
    static inline int subtractRedundant(int A);
    static inline bool isBetween(int a, int b, double c);
    static inline bool isIntersect(int a, int b, int c, int d);

private:
    static constexpr double EPS = 1E-9;
};

#endif // __GeometryUtils_hpp__
