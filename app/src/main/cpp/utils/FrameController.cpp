#include <opencv/cv.hpp>
#include "FrameController.hpp"

FrameController::FrameController(int displayOrientation) {
    _kSize = new cv::Size(3, 3);
    this->_displayOrientation = displayOrientation;
}


FrameController::~FrameController() {
    clear();
}


void FrameController::clear() {
    if (_currentFrame != nullptr) {
        delete _currentFrame;
    }

    if (_kSize != nullptr) {
        delete _kSize;
    }
}


std::vector<std::vector<cv::Point>> FrameController::getFrameContours(char *data, int width, int height) {
    _currentFrame = new cv::Mat(height, width, CV_8UC1, data);

    switch (_displayOrientation) {
        case 90:
            cv::transpose(*_currentFrame, *_currentFrame);
            cv::flip(*_currentFrame, *_currentFrame, 1);
            break;

        case 270:
            cv::transpose(*_currentFrame, *_currentFrame);
            cv::flip(*_currentFrame, *_currentFrame, 0);
            break;

        default:
            break;
    }
    cv::blur(*_currentFrame, *_currentFrame, *_kSize);

    cv::Mat cannyOutput;
    std::vector<std::vector<cv::Point>> contours;

    cv::Canny(*_currentFrame, cannyOutput, 0, 0);
    cv::findContours(cannyOutput, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

    return contours;
}
