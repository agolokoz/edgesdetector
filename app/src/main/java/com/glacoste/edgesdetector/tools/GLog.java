package com.glacoste.edgesdetector.tools;

import android.util.Log;

import java.util.Locale;

public class GLog {

    private static final String TAG = "EdgesDetector";
    private static boolean isRelease = false;

    public static void setIsRelease(boolean release) {
        isRelease = release;
    }

    /* --- Level: info --- */
    public static void info(String message) {
        if (!isRelease)
            Log.i(TAG, message);
    }

    public static void info(String format, Object... args) {
        if (!isRelease)
            Log.i(TAG, String.format(Locale.getDefault(), format, args));
    }
    /* ------------------- */


    /* --- Level: debug --- */
    public static void debug(String message) {
        if (!isRelease)
            Log.d(TAG, message);
    }

    public static void debug(String format, Object... args) {
        if (!isRelease)
            Log.d(TAG, String.format(Locale.getDefault(), format, args));
    }
    /* ------------------- */


    /* --- Level: info --- */
    public static void warn(String message) {
        if (!isRelease)
            Log.i(TAG, message);
    }

    public static void warn(String format, Object... args) {
        if (!isRelease)
            Log.i(TAG, String.format(Locale.getDefault(), format, args));
    }
    /* ------------------- */


    /* --- Level: info --- */
    public static void error(String message) {
        if (!isRelease)
            Log.i(TAG, message);
    }

    public static void error(String format, Object... args) {
        if (!isRelease)
            Log.i(TAG, String.format(Locale.getDefault(), format, args));
    }
    /* ------------------- */
}