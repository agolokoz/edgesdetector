package com.glacoste.edgesdetector;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.SurfaceView;
import android.view.View;

import com.glacoste.edgesdetector.camera.CameraController;
import com.glacoste.edgesdetector.draw.DrawView;
import com.glacoste.edgesdetector.tools.GLog;

public class MainActivity extends AppCompatActivity implements CameraController.CameraControllerEventListener {

    private static final boolean DEBUG = true;

    private CameraController cameraController;
    private DrawView drawView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.main_surface_view);
        drawView = (DrawView) findViewById(R.id.main_draw_view);

        cameraController = new CameraController(this);
        cameraController.setDisplaySurfaceView(surfaceView);
        cameraController.setBytesReceivedListener(this);

        setSystemUiVisibility();
    }

    @Override
    protected void onPause() {
        super.onPause();
        cameraController.releaseCamera();
    }

    @Override
    public void onCameraInit(int width, int height, int displayOrientation) {
        cameraInit(width, height, displayOrientation);
        if (displayOrientation == 90 || displayOrientation == 270) {
            drawView.setFrameSize(height, width);
        }
        else {
            drawView.setFrameSize(width, height);
        }
    }

    @Override
    public void onBytesReceived(byte[] bytes) {
        long start = System.currentTimeMillis();

        int[] array = findEdges(
                bytes,
                cameraController.getCameraFrameSize().getWidth(),
                cameraController.getCameraFrameSize().getHeight());

        drawView.drawLines(array);
        GLog.debug("onBytesReceived(): %d", System.currentTimeMillis()-start);
    }

    public void setSystemUiVisibility() {
        View decorView = getWindow().getDecorView();
        int uiFlags = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;

        if (Build.VERSION.SDK_INT >= 16)
            uiFlags |= View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;

        if (Build.VERSION.SDK_INT >= 19)
            uiFlags |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;

        decorView.setSystemUiVisibility(uiFlags);
    }

    static {
        System.loadLibrary("native-lib");
    }

    public static native void cameraInit(int width, int height, int displayOrientation);
    public static native int[] findEdges(byte[] byteArray, int width, int height);
}
