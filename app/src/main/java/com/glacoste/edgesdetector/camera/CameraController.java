package com.glacoste.edgesdetector.camera;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.glacoste.edgesdetector.tools.GLog;

import java.io.IOException;
import java.util.List;

@SuppressWarnings("deprecation")
public class CameraController implements
        SurfaceHolder.Callback,
        Camera.PreviewCallback {

    private final Context context;
    private SurfaceView surfaceView;
    private CameraControllerEventListener bytesReceivedListener;
    private Camera camera;

    private FrameSize cameraFrameSize;


    public CameraController(Context context) {
        this.context = context;
    }

    public void setDisplaySurfaceView(SurfaceView surfaceView) {
        this.surfaceView = surfaceView;
        surfaceView.getHolder().addCallback(this);
    }

    public void setBytesReceivedListener(CameraControllerEventListener bytesReceivedListener) {
        this.bytesReceivedListener = bytesReceivedListener;
    }

    public boolean initializeCamera() {
        if (surfaceView == null) {
            throw new RuntimeException("Display SurfaceView is null");
        }

        synchronized (this) {
            int cameraId = 0;
            camera = null;
            try {
                camera = Camera.open(cameraId);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (camera == null) {
                return false;
            }

            try {
                Camera.Parameters parameters = camera.getParameters();
                List<Camera.Size> previewSizeList = parameters.getSupportedPreviewSizes();
                if (previewSizeList == null) {
                    return false;
                }

                int viewWidth = surfaceView.getWidth();
                int viewHeight = surfaceView.getHeight();
                if (viewWidth < viewHeight) {
                    int temp = viewWidth;
                    viewWidth = viewHeight;
                    viewHeight = temp;
                }
                FrameSize frameSize = calculateCameraFrameSize(
                        previewSizeList,
                        viewWidth,
                        viewHeight);
                parameters.setPreviewSize(frameSize.getWidth(), frameSize.getHeight());
                GLog.info("Preview frame size: %dx%d", frameSize.getWidth(), frameSize.getHeight());

                List<String> focusModeList = parameters.getSupportedFocusModes();
                if (focusModeList != null && focusModeList.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
                }

                camera.setParameters(parameters);
                cameraFrameSize = new FrameSize(
                        parameters.getPreviewSize().width,
                        parameters.getPreviewSize().height);

                int displayOrientation = getCameraDisplayOrientation((Activity) context, cameraId, camera);
                camera.setDisplayOrientation(displayOrientation);

                bytesReceivedListener.onCameraInit(
                        cameraFrameSize.getWidth(),
                        cameraFrameSize.getHeight(),
                        displayOrientation);

                camera.setPreviewCallback(this);
                camera.setPreviewDisplay(surfaceView.getHolder());
                camera.startPreview();

            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        return true;
    }

    public void releaseCamera() {
        synchronized (this) {
            if (camera != null) {
                camera.stopPreview();
                try {
                    camera.setPreviewDisplay(null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                camera.setPreviewCallback(null);
                camera.release();
            }
            camera = null;
        }
    }

    public FrameSize getCameraFrameSize() {
        return cameraFrameSize;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        initializeCamera();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        releaseCamera();
        initializeCamera();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        releaseCamera();
    }

    private static FrameSize calculateCameraFrameSize(List<Camera.Size> supportedSizes, int surfaceWidth, int surfaceHeight) {
        int calcWidth = 0;
        int calcHeight = 0;
        double ratio = (double)surfaceWidth / surfaceHeight;

        surfaceWidth /= 1.5;
        surfaceHeight /= 1.5;

        for (Camera.Size size : supportedSizes) {
            if ((size.width <= surfaceWidth && size.height <= surfaceHeight)
                    && (size.width >= calcWidth && size.height >= calcHeight)
                    && (Double.compare((double)size.width / size.height, ratio) == 0)) {
                calcWidth = size.width;
                calcHeight = size.height;
            }
        }
        return new FrameSize(calcWidth, calcHeight);
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        if (bytesReceivedListener != null) {
            bytesReceivedListener.onBytesReceived(data);
        }
    }

    public int getCameraDisplayOrientation(Activity activity , int cameraId , Camera camera) {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, cameraInfo);

        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)  {
            result = (cameraInfo.orientation + degrees) % 360;
            result = (360 - result) % 360;
        }
        else {
            result = (cameraInfo.orientation - degrees + 360) % 360;
        }
        return result;
    }


    public static class FrameSize {

        private final int width;
        private final int height;

        public FrameSize(int width, int height) {
            this.width = width;
            this.height = height;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }
    }

    public interface CameraControllerEventListener {
        void onCameraInit(int width, int height, int displayOrientation);
        void onBytesReceived(byte[] bytes);
    }
}
