package com.glacoste.edgesdetector.draw;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import java.util.Arrays;
import java.util.Locale;

public class DrawView extends View {

    private static final int MAX_ANGLE_STATISTICS = 8 + 1;

    private Paint linesPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private FPSMeter fpsMeter;
    private int[] shapesArray;
    private int[] statisticsArray = new int [MAX_ANGLE_STATISTICS];

    private float xFactor = 1.f;
    private float yFactor = 1.f;

    public DrawView(Context context) {
        super(context);
        initialize();
    }

    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public DrawView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    private void initialize() {
        float density = getContext().getResources().getDisplayMetrics().density;

        linesPaint.setStrokeWidth(2 * density);
        linesPaint.setColor(0xff00ff00);

        textPaint.setColor(0xffff0000);
        textPaint.setTextSize(16.f * density);

        fpsMeter = new FPSMeter(density);
        fpsMeter.setTextPaint(textPaint);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (shapesArray != null && shapesArray.length >= 4) {
            Arrays.fill(statisticsArray, 0);

            int index = 1;
            while (index < shapesArray.length) {
                int shapeAngleCount = shapesArray[index-1];
                if (shapeAngleCount > 2) {
                    if (shapeAngleCount < statisticsArray.length) {
                        statisticsArray[shapeAngleCount] += 1;
                    }
                    for (int i = 1; i < shapeAngleCount; ++i) {
                        canvas.drawLine(
                                shapesArray[index + (i - 1) * 2] * xFactor,
                                shapesArray[index + (i - 1) * 2 + 1] * yFactor,
                                shapesArray[index + i * 2] * xFactor,
                                shapesArray[index + i * 2 + 1] * yFactor,
                                linesPaint);
                    }
                    canvas.drawLine(
                            shapesArray[index] * xFactor,
                            shapesArray[index + 1] * yFactor,
                            shapesArray[index + (shapeAngleCount - 1) * 2] * xFactor,
                            shapesArray[index + (shapeAngleCount - 1) * 2 + 1] * yFactor,
                            linesPaint
                    );
                }

                index += shapeAngleCount*2 + 1;
            }

            float top = textPaint.getTextSize() * 2 + 5;
            for (int i = 3; i < statisticsArray.length; ++i) {
                if (statisticsArray[i] > 0) {
                    canvas.drawText(
                            String.format(Locale.getDefault(), "%dangle: %d", i, statisticsArray[i]),
                            5,
                            top,
                            textPaint);
                    top += textPaint.getTextSize() + 5;
                }
            }
        }

        fpsMeter.drawingFinished(canvas);
        fpsMeter.drawingStarted();
    }

    public void drawLines(int[] pointsArray) {
        this.shapesArray = pointsArray;
        invalidate();
    }

    public void setFrameSize(int frameWidth, int frameHeight) {
        xFactor = (float)getWidth() / frameWidth;
        yFactor = (float)getHeight() / frameHeight;
    }
}
