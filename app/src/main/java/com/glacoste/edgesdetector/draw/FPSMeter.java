package com.glacoste.edgesdetector.draw;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Locale;

class FPSMeter {

    private static final int FPS_SHOW_PERIOD_MS = 1000;

    private final float density;

    private Paint   textPaint;

    private long    drawingStartTime    = 0;
    private long    drawTimeSumValue    = 0;
    private long    lastFpsShowTime     = 0;
    private int     drawTimeSumCount    = 0;

    private String fpsString;

    FPSMeter(float density) {
        this.density = density;
    }

    void setTextPaint(Paint textPaint) {
        this.textPaint = textPaint;
    }

    void drawingStarted() {
        drawingStartTime = getCurrentTime();
    }

    void drawingFinished(Canvas canvas) {
        drawTimeSumValue += (getCurrentTime() - drawingStartTime);
        drawTimeSumCount += 1;
        showFpsIfNeeded(canvas);
    }

    private void showFpsIfNeeded(Canvas canvas) {
        if (getCurrentTime() - lastFpsShowTime >= FPS_SHOW_PERIOD_MS) {
            float midDrawTime = Math.max(1, (drawTimeSumValue / drawTimeSumCount));
            float fpsValue = 1000.f / midDrawTime;
            fpsString = String.format(Locale.getDefault(), "%.2f", fpsValue);

            drawTimeSumCount = 0;
            drawTimeSumValue = 0;
            lastFpsShowTime = getCurrentTime();
        }

        if (textPaint != null) {
            canvas.drawText(fpsString, 5, textPaint.getTextSize(), textPaint);
        }
    }

    private long getCurrentTime() {
        return System.currentTimeMillis();
    }

}
